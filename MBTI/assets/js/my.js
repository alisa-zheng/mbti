function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != "function") {
        window.onload = func;
    } else {
        window.onload = function() {
            oldonload();
            func();
        };
    }
}


// 尔雅测评弹窗
function allHeight() {
    var h = $("body").children("div");
    n = h.length - 2
    for (var i = 0; i < n; i++) {
        var ah = $(this).outerHeight() * n;
        $("#popBg").css("height", ah);
    }
}

function popUp() {
    document.getElementById("popBg").style.display = "block";
    document.getElementById("popNav").style.display = "block";
    document.body.style.overflow = "hidden";
    $(window).scroll(function() {
        $(this).scrollTop(0)
    });
    $(document).bind("touchmove", function(e) {
        e.preventDefault();
    });
    allHeight();
}

function popDown() {
    document.getElementById("popBg").style.display = "none";
    document.getElementById("popNav").style.display = "none";
    document.body.style.overflow = "";
    $(window).unbind("scroll");
    $(document).unbind("touchmove");
}

// 引导页自适应
function selfAdaption() {
    var bodyWidth = document.body.offsetWidth;
    document.getElementById("leadText").style.top = bodyWidth / 3.6 + "px";
}
